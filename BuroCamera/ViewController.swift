//
//  ViewController.swift
//  BuroCamera
//
//  Created by sevenb on 2017-10-13.
//  Copyright © 2017 TheAppLabb. All rights reserved.
//


//Reference About CameraManager
//https://github.com/imaginary-cloud/CameraManager

import UIKit
import CameraManager

class ViewController: UIViewController {
	
	let cameraManager = CameraManager()
	
	// MARK: - @IBOutlets
	@IBOutlet weak var cameraView: UIView!
	@IBOutlet weak var cameraPermissionsView: UIView!
	@IBOutlet weak var cameraButton: UIButton!
	@IBOutlet weak var flashModeButton: UIButton!
	
	// MARK: - UIViewController
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		cameraManager.showAccessPermissionPopupAutomatically = false
		navigationController?.navigationBar.isHidden = true
		
		self.cameraButton.isHidden = true
		
		let currentCameraState = cameraManager.currentCameraStatus()
		
		if currentCameraState == .notDetermined || currentCameraState == .accessDenied{
			self.cameraPermissionsView.isHidden = false
			return
		}else if (currentCameraState == .ready) {
			self.cameraPermissionsView.isHidden = true
			self.addCameraToView()
		}

		if !cameraManager.hasFlash {
			flashModeButton.isEnabled = false
			flashModeButton.setImage(UIImage(named:"flash-off"), for: UIControlState())
			flashModeButton.setTitle("No flash", for: UIControlState())
			return
		}
		
		self.flashMode()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		navigationController?.navigationBar.isHidden = true
		cameraManager.resumeCaptureSession()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		cameraManager.stopCaptureSession()
	}
	
	//Adding Camera View
	fileprivate func addCameraToView()
	{
		self.cameraButton.isHidden = false
		
		_ = cameraManager.addPreviewLayerToView(cameraView, newCameraOutputMode: .stillImage)
		cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
			
			let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
			alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in  }))
			
			self?.present(alertController, animated: true, completion: nil)
		}
	}
	
	//Changing flash mode
	func flashMode(){
		switch (cameraManager.changeFlashMode()) {
		case .off:
			self.flashModeButton.alpha = 0.5
			self.flashModeButton.setImage(UIImage(named:"flash-off"), for: UIControlState())
		case .on:
			self.flashModeButton.alpha = 1.0
			self.flashModeButton.setImage(UIImage(named:"flash"), for: UIControlState())
		case .auto:
			self.flashModeButton.setImage(UIImage(named:"flash-auto"), for: UIControlState())
		}
	}
	
	
	// MARK: - @IBActions
	@IBAction func changeFlashMode(_ sender: UIButton){
		self.flashMode()
	}
	
	@IBAction func recordButtonTapped(_ sender: UIButton) {
		
		if cameraManager.cameraOutputMode == .stillImage {
			cameraManager.capturePictureWithCompletion({ (image, error) -> Void in
				if let errorOccured = error {
					self.cameraManager.showErrorBlock("Error occurred", errorOccured.localizedDescription)
				}
				else {
					self.performSegue(withIdentifier: "ImageViewController", sender: image)
				}
			})
		}
	}
	
	@IBAction func askForCameraPermissions(_ sender: UIButton) {

		cameraManager.askUserForCameraPermission({ permissionGranted in
			if permissionGranted {
				self.cameraPermissionsView.isHidden = true
				self.addCameraToView()
			}
		})
	}
	
	// MARK: - Segue
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == "ImageViewController" {
			let image:UIImage = sender as! UIImage
			let imageViewCt:ImageViewController = segue.destination as! ImageViewController
			imageViewCt.image = image
		}
	}
	
}

