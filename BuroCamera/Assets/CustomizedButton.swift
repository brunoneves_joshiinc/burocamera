//
//  CustomizedCameraButton.swift
//  BuroCamera
//
//  Created by sevenb on 2017-10-27.
//  Copyright © 2017 TheAppLabb. All rights reserved.
//

import UIKit

@IBDesignable open class CustomizedButton:UIButton {
	
	@IBInspectable var startColor: UIColor  = .clear
	@IBInspectable var endColor: UIColor 	= .clear
	
	// MARK: Lifecycle
	
	override open class var layerClass: AnyClass {
		return CAGradientLayer.self
	}
	
	@IBInspectable public var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
		}
	}
	
	@IBInspectable public var border: CGFloat {
		get {
			return layer.borderWidth
		}
		set {
			layer.borderWidth = newValue
		}
	}
	
	
	@IBInspectable public var borderColor: UIColor = .clear {
		didSet {
			layer.borderColor = borderColor.cgColor
		}
	}
	
	open override func layoutSublayers(of layer: CALayer) {
		super.layoutSublayers(of: layer)
		
		guard layer == self.layer else {
			return
		}
		(layer as! CAGradientLayer).colors = [startColor.cgColor, endColor.cgColor]
	}
}


