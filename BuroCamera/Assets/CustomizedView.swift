//
//  CustomizedView.swift
//  BuroCamera
//
//  Created by sevenb on 2017-10-27.
//  Copyright © 2017 TheAppLabb. All rights reserved.
//

import UIKit

@IBDesignable open class CustomizedView:UIView {
	@IBInspectable public var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
		}
	}
}
